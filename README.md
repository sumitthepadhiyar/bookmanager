This is a Book Manager Application. Entry class of the application is BooksManagerApplication(located /src/). The project was created in Eclipse IDE. If you do not use Eclipse IDE proceed as follows:
1. Application contains two packages - utils & model. First compile utils & then model.
2. Then compile BooksManager & then BooksManagerApplication.
Finally run BooksManagerApplication with two required arguments (--books, --sales) & three optional arguments(--top_selling_books, --top_customers, --sales_on_date).
Arguments should be provided in the form --argument_name=value. For --books & --sales file path of CSV containing respective data should be provided. 
If the path contains whitespaces please enclose the path within double quotes("").
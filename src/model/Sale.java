package model;

import java.util.Date;
import java.util.List;

public class Sale {

	public enum PaymentMode {
		CASH, CARD
	}

	private final Date date;
	private final String email;
	private final PaymentMode paymentMode;
	private final int count;
	private final List<BookPurchase> bookPurchases;

	// Total value of the sale which is summation individual book purchase.
	private float total;

	public Sale(Date date, String email, PaymentMode paymentMode, int count, List<BookPurchase> bookPurchases) {
		this.date = date;
		this.email = email;
		this.paymentMode = paymentMode;
		this.count = count;
		this.bookPurchases = bookPurchases;

		for (BookPurchase bp : bookPurchases) {
			total += bp.getTotal();
		}
	}

	public Date getDate() {
		return date;
	}

	public String getEmail() {
		return email;
	}

	public PaymentMode getPaymentMode() {
		return paymentMode;
	}

	public int getCount() {
		return count;
	}

	public List<BookPurchase> getBookPurchases() {
		return bookPurchases;
	}

	public float getTotal() {
		return total;
	}

	@Override
	public String toString() {
		return "Sale [date=" + date + ", email=" + email + ", paymentMode=" + paymentMode + ", count=" + count
				+ ", bookPurchases=" + toBookPurchaseString() + ", total=" + total + "]";
	}

	private String toBookPurchaseString() {
		StringBuilder sb = new StringBuilder("[");
		for (BookPurchase bp : bookPurchases) {
			sb.append(bp);
		}
		sb.append("]");
		return sb.toString();
	}

}

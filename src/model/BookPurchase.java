package model;

public class BookPurchase {

	private final String bookId;
	private final int quantity;

	// Preprocessed value = qunatity * book_price
	private final float total;

	public BookPurchase(String bookId, int quantity, float total) {
		this.bookId = bookId;
		this.quantity = quantity;
		this.total = total;
	}

	public String getBookId() {
		return bookId;
	}

	public int getQuantity() {
		return quantity;
	}

	public float getTotal() {
		return total;
	}

	@Override
	public String toString() {
		return "BookPurchase [bookId=" + bookId + ", quantity=" + quantity + ", total=" + total + "]";
	}

}

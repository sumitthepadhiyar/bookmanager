import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import model.Book;
import model.BookPurchase;
import model.Sale;
import model.Sale.PaymentMode;
import utils.CLIParser;

/**
 * Main class for BooksManagerApplication which contains business logic to
 * process query.
 */
public class BooksManager {

	private static String pattern;
	private static SimpleDateFormat simpleDateFormat;

	static {
		pattern = "yyyy-MM-dd";
		simpleDateFormat = new SimpleDateFormat(pattern);
	}

	/**
	 * Starts the BookManager query processing.
	 * 
	 * @param args
	 *            Command line args
	 */
	public static void startProcessing(String[] args) {
		Map<String, String> argsMap = CLIParser.parseArguments(args);

		if (argsMap.size() < 2) {
			throw new IllegalArgumentException("Insufficient arguments");
		}

		Map<String, Book> booksMap = readBookFromCSV(argsMap.get(CLIParser.BOOKS_ARG));
		List<Sale> sales = readSaleFromCSV(booksMap, argsMap.get(CLIParser.SALES_ARG));

		int i = 0;

		for (Map.Entry<String, String> e : argsMap.entrySet()) {
			i++;

			// Skip first two arguments which are file paths.
			if (i < 3) {
				continue;
			}

			switch (e.getKey()) {
			case CLIParser.TOP_SELLING_BOOKS_ARG:
				processTopSellingBooks(sales, argsMap.get(CLIParser.TOP_SELLING_BOOKS_ARG));
				break;
			case CLIParser.TOP_CUSTOMERS_ARG:
				processTopCustomers(sales, argsMap.get(CLIParser.TOP_CUSTOMERS_ARG));
				break;
			case CLIParser.SALES_ON_DATE_ARG:
				processSalesOnDate(sales, argsMap.get(CLIParser.SALES_ON_DATE_ARG));
				break;
			}
		}

	}

	/**
	 * Reads Book data from given file Path.
	 * 
	 * @param filePath
	 *            Book CSV file path.
	 * @return Map<String,Book> An map of book_id to Book.
	 * @throws IllegalArgumentException
	 */
	private static Map<String, Book> readBookFromCSV(String filePath) {
		File file = Paths.get(filePath).toFile();

		if (!file.exists()) {
			throw new IllegalArgumentException("Invalid Books CSV path");
		}

		HashMap<String, Book> argsMap = new HashMap<>();

		String line = null;
		String[] attributes;
		Book book;

		try (BufferedReader br = new BufferedReader(new FileReader(file))) {

			while ((line = br.readLine()) != null) {
				attributes = line.split(",");

				// Invalid Book CSV entry
				if (attributes.length < 4) {
					continue;
				}

				book = new Book(attributes[0], attributes[1], attributes[2], Float.parseFloat(attributes[3]));
				argsMap.put(book.getId(), book);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

		return argsMap;

	}

	/**
	 * Reads Sale data from given file Path.
	 * 
	 * @param filePath
	 *            Sale CSV file path
	 * @return List<Sale> List of Sale created from Sale's CSV.
	 * @throws IllegalArgumentException
	 */
	private static List<Sale> readSaleFromCSV(Map<String, Book> booksMap, String filePath) {
		File file = Paths.get(filePath).toFile();

		if (!file.exists()) {
			throw new IllegalArgumentException("Invalid Sale CSV path");
		}

		List<Sale> sales = new ArrayList<>();

		String line = null;
		Sale sale;

		try (BufferedReader br = new BufferedReader(new FileReader(file))) {

			while ((line = br.readLine()) != null) {

				sale = createSale(line, booksMap);

				/*
				 * If there was any problem in creating Sale object, null is
				 * returned. So instead of aborting program, that particular
				 * Sale row is not taken into account.
				 */
				if (sale == null) {
					continue;
				}

				sales.add(sale);

			}

		} catch (IOException e) {
			e.printStackTrace();
		}

		return sales;

	}

	/**
	 * Creates Sale object from {@code line} CSV row.
	 * 
	 * @param line
	 *            CSV row
	 * @param booksMap
	 *            An map of book_id to Book.
	 * @return List<Sale> List of Sale created from Sale's CSV.
	 * @throws IllegalArgumentException
	 */
	private static Sale createSale(String line, Map<String, Book> booksMap) {

		String[] args = line.split(",");

		// Invalid Sale CSV entry
		if (args.length < 5) {
			return null;
		}

		int noOfBooks = Integer.parseInt(args[3]);

		// Invalid books entry
		if (noOfBooks < 1 || args.length < (4 + noOfBooks)) {
			throw new IllegalArgumentException("Invalid no of books in entry -" + line);
		}

		BookPurchase bp;
		List<BookPurchase> booksPurchases = new ArrayList<>(noOfBooks);

		String[] bookDetails;
		Book book;
		int quantity;
		float bookValue;

		for (int i = 1; i <= noOfBooks; i++) {

			bookDetails = args[3 + i].split(";");

			// Error in book details
			if (bookDetails.length < 2) {
				throw new IllegalArgumentException(
						"Invalid Book Detail in  entry- " + args[3 + i] + ", of line-" + line);
			}

			// Retrieving Book by book_id.
			book = booksMap.get(bookDetails[0]);
			if (book == null) {
				throw new IllegalArgumentException("Invalid Book Id- " + bookDetails[0] + ", in line-" + line);
			}

			quantity = Integer.parseInt(bookDetails[1]);
			bookValue = quantity * book.getPrice();

			bp = new BookPurchase(bookDetails[0], quantity, bookValue);
			booksPurchases.add(bp);

		}

		Date date;

		try {
			date = simpleDateFormat.parse(args[0]);
		} catch (ParseException s) {
			// Logging invalid date & returning null.
			System.out.println("Invalid date-" + args[0] + " at line-" + line);
			return null;
		}

		return new Sale(date, args[1], PaymentMode.valueOf(args[2]), noOfBooks, booksPurchases);

	}

	/**
	 * Processes Top Selling books.
	 * 
	 * @param List<Sale>
	 *            List of Sale created from Sale's CSV.
	 * @param strCount
	 *            number of top selling books
	 */
	private static void processTopSellingBooks(List<Sale> sales, String strCount) {
		int count = Integer.parseInt(strCount);

		if (count < 1) {
			System.out.println("Count of top selling books should be greater than 0;");
			return;
		}

		// Grouping by book_id and then summing total sale value for each book
		Map<String, Double> bookIdValueMap = sales.stream().flatMap(sale -> sale.getBookPurchases().stream()).collect(
				Collectors.groupingBy(BookPurchase::getBookId, Collectors.summingDouble(BookPurchase::getTotal)));

		/*
		 * Sorting grouped map of book_id to total Sale value in descending
		 * order by total Sale value. Then converting it to list of book_id
		 */
		List<String> bookIds = bookIdValueMap.entrySet().stream()
				.sorted(Map.Entry.comparingByValue(Comparator.reverseOrder())).map(entry -> entry.getKey())
				.collect(Collectors.toList());

		System.out.print("top_selling_books");

		count = Math.min(count, bookIds.size());

		for (int i = 0; i < count; i++) {
			System.out.print("\t" + bookIds.get(i));
		}

		System.out.print("\n");

	}

	/**
	 * Processes Top Customers by book sale value.
	 * 
	 * @param List<Sale>
	 *            List of Sale created from Sale's CSV.
	 * @param strCount
	 *            number of top customers.
	 */
	private static void processTopCustomers(List<Sale> sales, String strCount) {

		int count = Integer.parseInt(strCount);

		if (count < 1) {
			System.out.println("Count of top customers should be greater than 0;");
			return;
		}

		// Grouping by customer's emaildId and then summing total sale value for
		// each customer
		Map<String, Double> emailSalesMap = sales.stream()
				.collect(Collectors.groupingBy(Sale::getEmail, Collectors.summingDouble(Sale::getTotal)));

		/*
		 * Sorting grouped map of emailId to total Sale value in descending
		 * order by total Sale value. Then converting it to list of emailId
		 */
		List<String> emails = emailSalesMap.entrySet().stream()
				.sorted(Map.Entry.comparingByValue(Comparator.reverseOrder())).map(entry -> entry.getKey())
				.collect(Collectors.toList());

		count = Math.min(count, emails.size());

		System.out.print("top_customers");

		for (int i = 0; i < count; i++) {
			System.out.print("\t" + emails.get(i));
		}

		System.out.print("\n");
	}

	/**
	 * Processes Sale on a particular date.
	 * 
	 * @param List<Sale>
	 *            List of Sale created from Sale's CSV.
	 * @param strDate
	 *            Date in string
	 */
	private static void processSalesOnDate(List<Sale> sales, String strDate) {
		Date date;
		try {
			date = simpleDateFormat.parse(strDate);
		} catch (ParseException s) {
			System.out.println("Invalid date-" + strDate);
			return;
		}

		float[] totalSaleValue = { 0f };

		// Filtering sales by particular date.
		sales.stream().filter(sale -> date.equals(sale.getDate()))
				.forEach(sale -> totalSaleValue[0] += sale.getTotal());

		System.out.println("sales_on_date\t" + strDate + "\t" + totalSaleValue[0]);

	}
}

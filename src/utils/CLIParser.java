package utils;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * An Command Line Argument Parser.
 */
public class CLIParser {

	public static final String BOOKS_ARG = "books";
	public static final String SALES_ARG = "sales";
	public static final String TOP_SELLING_BOOKS_ARG = "top_selling_books";
	public static final String TOP_CUSTOMERS_ARG = "top_customers";
	public static final String SALES_ON_DATE_ARG = "sales_on_date";

	public static Map<String, String> parseArguments(String[] args) {
		LinkedHashMap<String, String> argsMap = new LinkedHashMap<>(8);
		String[] splitString;

		for (String s : args) {

			if (s.length() < 2) {
				continue;
			}

			if (!"--".equals(s.substring(0, 2))) {
				continue;
			}

			splitString = s.split("=", 2);

			if (splitString.length < 2) {
				continue;
			}

			argsMap.put(splitString[0].trim().substring(2), splitString[1].trim());

		}

		return argsMap;
	}

}

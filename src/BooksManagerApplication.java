/**
 * Entry point for BooksManagerApplication
 */
public class BooksManagerApplication {
	public static void main(String[] args) {
		BooksManager.startProcessing(args);
	}
}
